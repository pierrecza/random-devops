# random-devops

## TODO

- [x] ansible deploy bulid prod sur devops1
  - voir pour la mise en place de vault pour secure les mdp d'escalation privilege
  - en fait pas besoin d'escalation de privilege puisque le but de ansible c'est de deploy sur une nouvelle machine, donc en root.. C'est lui qui créera le nouveau user etc
- [x] config traefik pour accéder correctement au site [web](devops1.curzola.me)
  - créer un docker network
- [ ] mise en place de minio
- [x] mise en place de prometheus + grafana


## Prérequis ansible

- Avoir déjà setup un accès a la machine avec une clé ssh.
- Générer un clé ssh locale: 
```bash
ssh-keygen -t rsa -b 4096 -C "email@example.com"
```
- créer un fichier `./ansible-playbook/.vault_pass` contenant le mot de passe de chiffrement des credentials

## ssh key
faut que la key s'appelle `id_rsa`

## best practices
- utiliser import tasks pour définir des blocs dans des fichiers


## test la connexion

depuis le dossier `./ansible-playbook`
`ansible all -m ping`

## installer un environement sur un nouvelle machine

depuis le dossier `./ansible-playbook`
Ajouter le moyen de connexion à la machine dans le fichier `./hosts`

```bash
ansible-playbook ./playbook_setup.yml -l SERVER_EXAMPLE --extra-vars "ansible_sudo_pass={{ vault_sudo_password }}"
ansible-playbook ./playbook_docker.yml -l SERVER_EXAMPLE --extra-vars "ansible_sudo_pass={{ vault_sudo_password }}"
```

## code source
`https://github.com/blackfoot-makers/devOpsTuto/tree/workshop/day1`

# CI/CD

## best practices
- utiliser `npm ci` à la place de `npm i` ça permet de ne pas créer un nouveau package-lock mais de vérifier si les node_modules sont synchro avec le package-lock existant

# Docker
`docker-compose -f docker-compose.yml -f docker-compose.local.yml up --build`

# Use Makefile in devOpsProject

## Release
### Build
```
make build
```
### up
```
make up
```

## Dev
### Build
```
make build_dev
```

### up
```
make up_dev
```