FROM node:12

RUN yarn global add serve --prefix /usr/local

#RUN mkdir -p /root/workspace
WORKDIR /MyProj-Frontend

COPY . .
RUN yarn
RUN yarn build
HEALTHCHECK CMD curl --fail http://localhost:3000/ || exit 1
#CMD ["yarn", "start"]
CMD /bin/bash -c "serve -s build -l 3000"
EXPOSE 3000
