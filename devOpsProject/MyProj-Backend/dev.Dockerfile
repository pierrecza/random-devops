FROM node

RUN npm install -g prisma
RUN npm install nodemon -g
WORKDIR /MyProj-Backend
#COPY . .
#RUN npm install
#RUN npm install --save prisma-client-lib
CMD /bin/bash -c "npm install && prisma deploy && prisma generate && nodemon --inspect ./index.js"
#CMD /bin/bash -c "npm install && prisma deploy && prisma generate && nodemon --inspect ./index.js"
