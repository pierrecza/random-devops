FROM node

RUN npm install -g prisma
#RUN npm install nodemon -g
WORKDIR /MyProj-Backend
COPY . .
RUN npm install
HEALTHCHECK CMD curl --fail http://localhost:4000/ && curl --fail http://prisma:4466 || exit 1
#RUN npm install --save prisma-client-lib
CMD /bin/bash -c "prisma deploy && prisma generate && node index.js"
#CMD /bin/bash -c "npm install && prisma deploy && prisma generate && nodemon --inspect ./index.js"
